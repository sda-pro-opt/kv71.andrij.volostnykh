#include "TokenAnalyzer.h"

void TokenAnalyzer::grouping() // this function marks all symbols in array as ascii groups (like 0 is spaces and 1 is capital letters)
{
	for (int i = 0; i < 128; i++) {
		if ((i == 9) || (i == 10) || (i == 32) || (i == 12) || (i == 13))
			ascii_arr[i] = 0;
		else {
			if ((i < 91) && (i > 64))
				ascii_arr[i] = 1;
			else {
				if ((i < 58) && (i > 47) || (i == 45))
					ascii_arr[i] = 2;
				else {
					if (i == ':')
						ascii_arr[i] = 3;
					else {
						if ((i == ')') || (i == '(') || (i == '*') || (i == '.') || (i == ';') || (i == '='))
							ascii_arr[i] = 4;
						else {
							ascii_arr[i] = 5;
						}
					}
				}
			}
		}
	}
}


void TokenAnalyzer::AddToken(int code, int line, int column, string name)
{
	Token buf;
	buf.code = code;
	buf.line = line;
	buf.column = column;
	buf.name = name;
	tokens.push_back(buf);
}

bool TokenAnalyzer::SizeOut()
{
	return !file.eof();
}

void TokenAnalyzer::INP()
{
	while (SizeOut())
	{
		switch (ascii_arr[sbuff])
		{
		case 0:
			SPACE();
			break;
		case 1:
			IDN();
			break;
		case 2:
			Digit();
			break;
		case 3:
			MDM();
			break;
		case 4:
			DM();
			break;
		case 5:
			ERR("");
			break;
		}
	}

}

void TokenAnalyzer::SPACE()
{
	if (SizeOut())
	{
		while ((SizeOut()) && (ascii_arr[sbuff] == 0))
		{
			if (sbuff == 10)
			{
				position++; column_counter++;
				sbuff = file.get();
				line_counter++;
				column_counter = 1;
			}
			else {
				position++; column_counter++;
				sbuff = file.get();
			}
		}
		return;
	}
}

void TokenAnalyzer::Digit() {
	save_line = line_counter;
	save_column = column_counter;
	string buf = "";
	while ((SizeOut()) && (ascii_arr[sbuff] == 2))
	{
		buf += sbuff;
		position++;
		sbuff = file.get();
		column_counter++;
	}

	switch (ascii_arr[sbuff])
	{
	case 0:
	case 3:
	case 4:
	{
		int n = SearchDigit(buf);
		if (n == -1)
		{
			AddToken(digit_counter, save_line, save_column, buf);
			numbers_table.push_back(buf);
			digit_counter++;
		}
		else
		{
			AddToken(n + min_digit, save_line, save_column, buf);
		}
		buf = "";
		return;
	}
	break;
	case 1:
	case 5:
		ERR(buf);
		break;
	}
}

void TokenAnalyzer::IDN() {
	int n;
	save_line = line_counter;
	save_column = column_counter;
	string Buf = "";
	while ((SizeOut()) && ((ascii_arr[sbuff] == 2) || (ascii_arr[sbuff] == 1)))
	{
		Buf += sbuff;
		position++;
		sbuff = file.get();
		column_counter++;
	}

	if (ascii_arr[sbuff] == 5)
	{
		ERR(Buf);
	}
	else
	{
		n = SearchStandartIdent(Buf);
		if (n == -1) {
			n = SearchIdent(Buf);
			if (n == -1)
			{
				AddToken(idn_counter, save_line, save_column, Buf);
				idn_table.push_back(Buf);
				idn_counter++;
			}
			else
			{
				AddToken(n + min_idn, save_line, save_column, Buf);
			}
		}
		else AddToken(key_words[n].code, save_line, save_column, key_words[n].name);
		Buf = "";
		return;
	}
}

void TokenAnalyzer::BCOM()
{
	position++;
	sbuff = file.get();
	column_counter++;
	COM("(");
}

void TokenAnalyzer::COM(string Buf)
{
	while (SizeOut() && (sbuff != '*'))
	{
		if (sbuff == '\n')
		{
			line_counter++;
			column_counter = 1;
			position++;
			sbuff = file.get();
		}
		else {
			Buf += sbuff;
			position++;
			sbuff = file.get();
			column_counter++;
		}
	}
	if (!SizeOut())
		ERR(Buf);
	else ECOM(Buf);
}

void TokenAnalyzer::ECOM(string Buf)
{
	sbuff = file.get();
	position++;
	column_counter++;
	if ((SizeOut()) && (sbuff == ')'))
	{
		position++;
		sbuff = file.get();
		column_counter++;
		return;
	}
	else if (!SizeOut())
		ERR(Buf);
	else COM(Buf);
}

void TokenAnalyzer::MDM()
{
	int n;
	save_line = line_counter;
	save_column = column_counter;
	string Buf = "";
	while ((SizeOut()) && ((ascii_arr[sbuff] == 3) || (ascii_arr[sbuff] == 4)))
	{
		Buf += sbuff;
		position++;
		sbuff = file.get();
		column_counter++;
	}

	if (ascii_arr[sbuff] == 5)
	{
		ERR(Buf);
	}
	else
	{
		n = SearchMDM(Buf);
		if (n == -1) {
			ERR(Buf);
		}
		else AddToken(mdm_table[n].code, save_line, save_column, mdm_table[n].name);
		Buf = "";
		return;
	}
}

void TokenAnalyzer::DM()
{
	save_line = line_counter;
	save_column = column_counter;
	string Buf = "";

	if (sbuff == '(')
	{
		Buf += sbuff;
		position++;
		sbuff = file.get();
		column_counter++;
		if (sbuff == '*') {
			BCOM();
		}
		else {
			AddToken('(', line_counter, column_counter - 1, Buf);
			return;
		}
	}
	else {
		if (sbuff == '*')
		{
			Buf += sbuff;
			ERR(Buf);
		}
		else
		{
			Buf += sbuff;
			AddToken(sbuff, line_counter, column_counter, Buf);
			position++;
			sbuff = file.get();
			column_counter++;
			return;
		}
	}
}

void TokenAnalyzer::ERR(string pt)
{
	string Buf = pt;
	if ((sbuff == '*') || (sbuff == ')'))
	{
		Buf += sbuff;
		AddToken(token_error_code, line_counter, column_counter, Buf);
		position++;
		sbuff = file.get();
		column_counter++;
		Buf = "";
		INP();
	}
	else
	{
		while ((ascii_arr[sbuff] != 0) && (ascii_arr[sbuff] != 3) && (SizeOut()))
		{
			Buf += sbuff;
			position++;
			sbuff = file.get();
			column_counter++;
		}

		AddToken(token_error_code, save_line, save_column, Buf);
		Buf = "";
		INP();
	}
}

int TokenAnalyzer::SearchIdent(string Ident) {
	for (int i = 0; i < idn_table.size(); i++)
	{
		if (idn_table[i] == Ident)
			return i;
	}
	return -1;
}

int TokenAnalyzer::SearchStandartIdent(string Ident) {
	for (int i = 0; i < key_words.size(); i++)
	{
		if (key_words[i].name == Ident)
			return i;
	}
	return -1;
}

int TokenAnalyzer::SearchDigit(string Digit) {
	for (int i = 0; i < numbers_table.size(); i++)
	{
		if (numbers_table[i] == Digit)
			return i;
	}
	return -1;
}

int TokenAnalyzer::SearchMDM(string MDM) {
	for (int i = 0; i < mdm_table.size(); i++)
	{
		if (mdm_table[i].name == MDM)
			return i;
	}
	return -1;
}

void TokenAnalyzer::MakeListing(string file) {
	file = file + "listing.txt";
	ofstream f(file);
	f << setw(10) << "code" << setw(10) << "line" << setw(10) << "column" << setw(20) << "name" << endl;
	f << endl;
	for (int i = 0; i < tokens.size(); i++)
	{
		f << setw(10) << tokens[i].code << setw(10) << tokens[i].line << setw(10) << tokens[i].column << setw(20) << tokens[i].name << endl;
	}
	f << endl;
	f << endl;
	int p = 0;
	for (int i = 0; i < tokens.size(); i++)
	{
		if (tokens[i].code == token_error_code) {
			f << "Undefined token in line: " << tokens[i].line << " column " << tokens[i].column << ": " << tokens[i].name << endl;
			p++;
		}
	}
	f.close();
	if (p == 0) {
		cout << "Token analysing finished" << endl;
	}
	else {
		for (unsigned int i = 0; i < tokens.size(); i++)
		{
			if (tokens[i].code == token_error_code) {
				cout << "Undefined token in line: " << tokens[i].line << " column " << tokens[i].column << ": " << tokens[i].name << endl;
			}
		}
	}
}

TokenAnalyzer::TokenAnalyzer()
{
	// initialize required veriables 
	token_counter = 1;
	line_counter = 1;
	column_counter = 1;
	token_error_code = 2000;
	idn_counter = min_idn;
	min_idn = 1001;
	digit_counter = min_digit;
	min_digit = 501;
	digit_position;
	keywords_min = 401;

	// adding our keywords to vector
	buffer.name = "PROGRAM";
	buffer.code = 401;
	key_words.push_back(buffer);

	buffer.name = "BEGIN";
	buffer.code = 402;
	key_words.push_back(buffer);

	buffer.name = "END";
	buffer.code = 403;
	key_words.push_back(buffer);

	buffer.name = "CONST";
	buffer.code = 404;
	key_words.push_back(buffer);

	buffer.name = ":=";
	buffer.code = 301;
	mdm_table.push_back(buffer);

	grouping();
}

void TokenAnalyzer::Analyzer(string filename)
{
	file.open(filename + "input.sig");
	if (!file.is_open()) {
		std::cout << "failed to open " << filename << '\n';
	}
	position = 0;
	line_counter = 1;
	column_counter = 1;
	sbuff = file.get();
	if (SizeOut())
		INP();
	MakeListing(filename);
}